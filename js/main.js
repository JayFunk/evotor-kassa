var app = new Vue({
  el: '#app',
  data: {
    cashboxComplectPrice: 0,
    cashboxComplectTitle: 'Смарт-терминал',
    cashboxComplectImage: 'img/evotor 5/icons/calc-0.png',
    selectedCashboxComplect: {},
    selectedCashbox: {},
    selectedFN: '',
    selectedScaner: '',
    selectedAlko: '',
    selectedTerminal: '',
    prevSelected:{
      selectedFN: '',
      SelectedScaner: '',
      SelectedAlko: '',
      selectedTerminal: ''
    },
    isAlkoSelected: false,
    fn13: {
      price: 6000,
      id: 1
    },
    fn36: {
      price: 9000,
      id: 2
    },
    scaner1D: {
      price: 1500,
      id: 4
    },
    scaner2D: {
      price: 6500,
      id: 8
    },
    alko1: {
      price: 10990,
      id: 16
    },
    alko2: {
      price: 5500,
      id: 32
    },
    twocan: {
      price: 7990,
      id: 64,
      title: '2can P17'
    },
    pax: {
      price: 7500,
      id: 128,
      title: 'PAX SP30'
    },
    cashboxComplects: [
      {
        sum: 0,
        title: 'Смарт-терминал',
        img: '/icons/calc-0.png'
      },

      {
        sum: 1,
        title: 'Смарт-терминал ФН13',
        img: '/icons/calc-1.png'
      },
      {
        sum: 5,
        title: 'Стандарт ФН13',
        img: '/icons/calc-5.png'
      },
      {
        sum: 9,
        title: 'Стандарт Плюс ФН',
        img: '/icons/calc-9.png'
      },
      {
        sum: 17,
        title: 'Алко ФН без сканера (УТМ)',
        img: '/icons/calc-17.png'
      },
      {
        sum: 33,
        title: 'Алко ФН без сканера (УТМ HUB)',
        img: '/icons/calc-33.png'
      },
      {
        sum: 25,
        title: 'Алко ФН (УТМ)',
        img: '/icons/calc-25.png'
      },
      {
        sum: 41,
        title: 'Алко ФН (УТМ HUB)',
        img: '/icons/calc-41.png'
      },

      {
        sum: 2,
        title: 'Смарт-терминал ФН36',
        img: '/icons/calc-2.png'
      },
      {
        sum: 6,
        title: 'Стандарт ФН36',
        img: '/icons/calc-6.png'
      },
      {
        sum: 10,
        title: 'Стандарт Плюс ФН36',
        img: '/icons/calc-10.png'
      },


      {
        sum: 4,
        title: 'Стандарт без ФН',
        img: '/icons/calc-4.png'
      },


      {
        sum: 8,
        title: 'Стандарт Плюс без ФН',
        img: '/icons/calc-8.png'
      },
      {
        sum: 24,
        title: 'Алко без ФН (УТМ)',
        img: '/icons/calc-24.png'
      },
      {
        sum: 40,
        title: 'Алко без ФН (УТМ HUB)',
        img: '/icons/calc-40.png'
      },

      {
        sum: 16,
        title: 'Алко без ФН и Сканера (УТМ)',
        img: '/icons/calc-16.png'
      },
      {
        sum: 32,
        title: 'Алко без ФН и Сканера (УТМ HUB)',
        img: '/icons/calc-32.png'
      }
    ],
    cashboxes: [
      {
        id: 100,
        title: 'Эвотор 5',
        price: 13890,
        imgPath: 'img/evotor 5'
      },
      {
        id: 200,
        title: 'Эвотор 7.2',
        price: 11990,
        imgPath: 'img/evotor 5'
      },
      {
        id: 300,
        title: 'Эвотор 7.3',
        price: 23490,
        imgPath: 'img/evotor 5'
      },
      {
        id: 400,
        title: 'Эвотор 10',
        price: 28990,
        imgPath: 'img/evotor 5'
      }
    ]
  },
  watch: {
    selectedFN: function (){

      this.updateCashboxPrice()
    },
    selectedScaner: function (){
      this.updateCashboxPrice()
    },
    selectedAlko: function (){
      this.updateCashboxPrice()
    },
    selectedTerminal: function (){
      this.updateCashboxPrice()
    }
  },
  methods: {
    getCashboxData: function (id) {
      return this.cashboxes.find(x => x.id === id)
    },
    selectCashbox: function (id) {
      console.log('select cashbox');
      this.selectedCashbox = this.cashboxes.find(x => x.id === id)

      this.selectedFN = ''
      this.selectedScaner = ''
      this.selectedAlko = ''
      this.selectedTerminal = ''
      this.updateCashboxPrice()
    },
    updateCashboxPrice: function(){
      console.log('-------------------')
      console.log('updateCashboxPrice');
      var price = this.selectedCashbox.price
      var idsSum = 0
      if(this.selectedFN != ''){
        price += this[this.selectedFN].price
        idsSum += this[this.selectedFN].id
      }
      console.log(this.selectedFN)
      if(this.selectedScaner != ''){
        price += this[this.selectedScaner].price
        idsSum += this[this.selectedScaner].id
      }
      if(this.selectedTerminal != ''){
        price += this[this.selectedTerminal].price
        // idsSum += this[this.selectedScaner].id
      }
      console.log(this.selectedScaner)
      if(this.selectedAlko != ''){
        price += this[this.selectedAlko].price
        idsSum += this[this.selectedAlko].id
        console.log('check alko')
        console.log(this.isAlkoSelected == false)
        if(this.isAlkoSelected == false){
          console.log('first check')
          this.isAlkoSelected = true
          this.selectedScaner = 'scaner2D'
          this.prevSelected.selectedScaner = 'scaner2D'
          this.selectedFN = 'fn13'
          this.prevSelected.selectedFN = 'fn13'
        }else{
          console.log('second check')
          this.isAlkoSelected = true
        }
      }else{
        this.isAlkoSelected = false
      }
      console.log(this.selectedAlko)
      this.cashboxComplectPrice = price
      console.log(this.cashboxComplectPrice)
      this.selectedCashboxComplect = this.cashboxComplects.find(x => x.sum === idsSum)
    },
    checkCashboxModalRadio: function(type, value){
      Vue.nextTick(() => {
        if(this[type] == this.prevSelected[type]){
          this[type] = ''
          this.prevSelected[type] = ''
        }else{
          this.prevSelected[type] = value
        };
      })
    },
    selectedCashboxImgPath: function(){
      this.selectedCashboxImgPath = this.selectedCashbox.imgPath
    }
  },
  computed: {
    selectedTerminalTitle: function(){
      if(this.selectedTerminal != ''){
        return this[this.selectedTerminal].title
      }else{
        return false
      }
    },
    calcImg: function(){
      console.log('call calcImg')
      if(this.selectedCashbox.imgPath + this.selectedCashboxComplect.img){
        return this.selectedCashbox.imgPath + this.selectedCashboxComplect.img
      }else{
        return this.cashboxComplectImage
      }
    },
    isNotSelectedCashbox: function(){
      if(Object.keys(this.selectedCashbox).length == 0){
        return true
      }else{
        return false
      }
    }
  }
})

$(document).ready(function(){
  $("#preloader").addClass("loaded");
  var instance = $('.lazy').Lazy({
    scrollDirection: 'vertical',
    effect: 'fadeIn',
    visibleOnly: true,
    chainable: false,
    onError: function(element) {
        console.log('error loading ' + element.data('src'));
    }
  });
  $('body').on('click', '.cashbox', function (e) {
    console.log('open modal')
    setTimeout(function() {instance.update()}, 1000);
  })
  $('#cashbox-modal__carousel_5').on('slide.bs.carousel', function () {
    setTimeout(function() {instance.update()}, 400);
  })
  $('#cashbox-modal__carousel_7_2').on('slide.bs.carousel', function () {
    setTimeout(function() {instance.update()}, 400);
  })
  $('#cashbox-modal__carousel_7_3').on('slide.bs.carousel', function () {
    setTimeout(function() {instance.update()}, 400);
  })
  $('#cashbox-modal__carousel_10').on('slide.bs.carousel', function () {
    setTimeout(function() {instance.update()}, 400);
  })
  $.stellar({
    horizontalScrolling: false,
    verticalScrolling: true,
    horizontalOffset: 0,
    verticalOffset: 0,
  });

  $('.services-carousel').owlCarousel({
    margin:10,
    loop:true,
    autoWidth:true,
    items:12,
    nav: false,
    dots: false,
    lazyLoad: true
  })

  $('.services-tech-carousel').owlCarousel({
    margin:10,
    loop:true,
    autoWidth:true,
    items:12,
    nav: false,
    dots: false,
    lazyLoad: true
  })

  $('#lifehacks a').click(function (e) {
    var selectedTab = $(this).attr('data-active')
    console.log(selectedTab)
    $('#lifehacksSection .lifehackBg').removeClass('active')
    $('#lifehacksSection .lifehackBg#lifehackBg-'+selectedTab).addClass('active')
  })

  $('.navbar a').click(function(){
      $('#navbarText').collapse('hide');
      $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top
      }, 500);
      return false;
  });

  $('body').on('click', '.cashbox-modal__calc #InfratelWidget button.btn', function(){
      $('.modal').animate({
          scrollTop: 0
      }, 500);
      return false;
  });
})

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    // console.log(scroll/10 + 'px');
    // console.log(scroll/10 + 'px !important');
    if(scroll/10 > 100){
      $('.header__img').css('left', scroll/-10 + 'px');
    }else{
      $('.header__img').css('left', scroll/-10 + 'px');
    }
});
