// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player5;
var player72;
var player73;
var player10;
function onYouTubeIframeAPIReady() {
  player5 = new YT.Player('evotot-5__video', {
    height: '500',
    width: '640',
    videoId: 'yDY-Stkkj44',
    playerVars: {
      rel:0,
      controls: 0,
      showinfo:0
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player72 = new YT.Player('evotot-72__video', {
    height: '500',
    width: '640',
    videoId: 'PDO7DL_HzS8',
    playerVars: {
      rel:0,
      controls: 0,
      showinfo:0
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player73 = new YT.Player('evotot-73__video', {
    height: '500',
    width: '640',
    videoId: 'DNXv3GiG97I',
    playerVars: {
      rel:0,
      controls: 0,
      showinfo:0
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player10 = new YT.Player('evotot-10__video', {
    height: '500',
    width: '640',
    videoId: 'lmXwJvvRsFo',
    playerVars: {
      rel:0,
      controls: 0,
      showinfo:0
    },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });

  // 4. The API will call this function when the video player is ready.
  function onPlayerReady(event) {
    // event.target.playVideo();
  }

  // 5. The API calls this function when the player's state changes.
  //    The function indicates that when playing a video (state=1),
  //    the player should play for six seconds and then stop.
  var done = false;
  function onPlayerStateChange(event) {
    // if (event.data == YT.PlayerState.PLAYING && !done) {
    //   setTimeout(stopVideo, 6000);
    //   done = true;
    // }
  }
  function stopVideo() {
    // player.stopVideo();
  }
}
